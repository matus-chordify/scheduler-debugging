#!/bin/bash

set -xeuo pipefail

stage_number="$1"
sleep_sec="$2"
experiment_id="$(cat experiment-id.txt)"

echo "\"$(date '+%Y-%m-%d %H:%M:%S')\",${experiment_id},${CI_COMMIT_REF_NAME},${stage_number},start" >> /tmp/stages.csv
sleep "${sleep_sec}"
echo "\"$(date '+%Y-%m-%d %H:%M:%S')\",${experiment_id},${CI_COMMIT_REF_NAME},${stage_number},end" >> /tmp/stages.csv
