#!/bin/bash

set -xeuo pipefail

experiment_id="$1"

git checkout master
git branch -D branchA branchB branchC branchD || true

# make some change in master
echo "$experiment_id" > experiment-id.txt
git commit -m "start ${experiment_id}" experiment-id.txt || true

git push -f origin master
sleep 20

git checkout -b branchA
git push -f -u origin branchA
sleep 20

git checkout -b branchB
git push -f -u origin branchB
sleep 60

git checkout -b branchC
git push -f -u origin branchC
sleep 30

git checkout -b branchD
git push -f -u origin branchD
sleep 30

git checkout master
